/*
 * Copyright 2022 Advanced Micro Devices, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Authors: Tom St Denis <tom.stdenis@amd.com>
 *
 */
#include "umr.h"
#include <linux/ioctl.h>
#include <linux/types.h>
#include <asm/ioctl.h>
#include <sys/ioctl.h>

// TODO: re-enable mmio support
#if 0
static void wave_read_regs_via_mmio(struct umr_asic *asic, uint32_t simd,
			   uint32_t wave, uint32_t thread,
			   uint32_t regno, uint32_t num, uint32_t *out)
{
	struct umr_reg *ind_index, *ind_data;
	uint32_t data;

	ind_index = umr_find_reg_data_by_ip_by_instance(asic, "gfx", asic->options.vm_partition, "mmSQ_IND_INDEX");
	ind_data  = umr_find_reg_data_by_ip_by_instance(asic, "gfx", asic->options.vm_partition, "mmSQ_IND_DATA");

	if (ind_index && ind_data) {
		data = umr_bitslice_compose_value(asic, ind_index, "WAVE_ID", wave);
		data |= umr_bitslice_compose_value(asic, ind_index, "INDEX", regno);
		if (asic->family < FAMILY_NV) {
			data |= umr_bitslice_compose_value(asic, ind_index, "THREAD_ID", thread);
			data |= umr_bitslice_compose_value(asic, ind_index, "FORCE_READ", 1);
			data |= umr_bitslice_compose_value(asic, ind_index, "SIMD_ID", simd);
		} else {
			data |= umr_bitslice_compose_value(asic, ind_index, "WORKITEM_ID", thread);
		}
		data |= umr_bitslice_compose_value(asic, ind_index, "AUTO_INCR", 1);
		umr_write_reg(asic, ind_index->addr * 4, data, REG_MMIO);
		while (num--)
			*(out++) = umr_read_reg(asic, ind_data->addr * 4, REG_MMIO);
	} else {
		asic->err_msg("[BUG]: The required SQ_IND_{INDEX,DATA} registers are not found on the asic <%s>\n", asic->asicname);
		return;
	}
}
#endif

struct amdgpu_debugfs_gprwave_iocdata {
	__u32 gpr_or_wave, se, sh, cu, wave, simd, xcc_id;
	struct {
		__u32 thread, vpgr_or_sgpr;
	} gpr;
};


enum AMDGPU_DEBUGFS_GPRWAVE_CMDS {
	AMDGPU_DEBUGFS_GPRWAVE_CMD_SET_STATE=0,
};
#define AMDGPU_DEBUGFS_GPRWAVE_IOC_SET_STATE _IOWR(0x20, AMDGPU_DEBUGFS_GPRWAVE_CMD_SET_STATE, struct amdgpu_debugfs_gprwave_iocdata)

static int read_gpr_gprwave(struct umr_asic *asic, int v_or_s, uint32_t thread, struct umr_wave_data *wd, uint32_t *dst)
{
	struct amdgpu_debugfs_gprwave_iocdata id;
	int r = 0;
	uint32_t size;
	uint64_t addr = 0;

	memset(&id, 0, sizeof id);
	id.gpr_or_wave = 1;
	if (asic->family < FAMILY_NV) {
		id.se = umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_HW_ID", "SE_ID");
		id.sh = umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_HW_ID", "SH_ID");
		id.cu = umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_HW_ID", "CU_ID");
		id.wave = umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_HW_ID", "WAVE_ID");
		id.simd = umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_HW_ID", "SIMD_ID");

		if (v_or_s == 0) {
			uint32_t shift;
			id.gpr.thread = 0;
			if (asic->family <= FAMILY_CIK)
				shift = 3;  // on SI..CIK allocations were done in 8-dword blocks
			else
				shift = 4;  // on VI allocations are in 16-dword blocks
			size = 4 * ((umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_GPR_ALLOC", "SGPR_SIZE") + 1) << shift);
		} else {
			id.gpr.thread = thread;
			size = 4 * ((umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_GPR_ALLOC", "VGPR_SIZE") + 1) << asic->parameters.vgpr_granularity);
		}
	} else {
		id.se = umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_HW_ID1", "SE_ID");
		id.sh = umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_HW_ID1", "SA_ID");
		id.cu = ((umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_HW_ID1", "WGP_ID") << 2) | umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_HW_ID1", "SIMD_ID"));
		id.wave = umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_HW_ID1", "WAVE_ID");
		id.simd = 0;
		if (v_or_s == 0) {
			id.gpr.thread = 0;
			size = 4 * 124; // regular SGPRs, VCC, and TTMPs
		} else {
			id.gpr.thread = thread;
			size = 4 * ((umr_wave_data_get_bits(asic, wd, "ixSQ_WAVE_GPR_ALLOC", "VGPR_SIZE") + 1) << asic->parameters.vgpr_granularity);
		}
	}
	id.gpr.vpgr_or_sgpr = v_or_s;
	id.xcc_id = asic->options.vm_partition == -1 ? 0 : asic->options.vm_partition;

	r = ioctl(asic->fd.gprwave, AMDGPU_DEBUGFS_GPRWAVE_IOC_SET_STATE, &id);
	if (r)
		return r;

	lseek(asic->fd.gprwave, 0, SEEK_SET);
	r = read(asic->fd.gprwave, dst, size);
	if (r < 0)
		return r;

	// if we are reading SGPRS then optionally dump them
	// and then read TRAP registers if necessary
	if (asic->options.test_log && asic->options.test_log_fd) {
		int x;

		// we use addr for test logging
		addr =
			((v_or_s ? 0ULL : 1ULL) << 60) | // reading SGPRs
			((uint64_t)0)                  | // starting address to read from
			((uint64_t)id.se << 12)        |
			((uint64_t)id.sh << 20)        |
			((uint64_t)id.cu << 28)        |
			((uint64_t)id.wave << 36)      |
			((uint64_t)id.simd << 44)      |
			((uint64_t)thread << 52ULL); // thread_id

		fprintf(asic->options.test_log_fd, "%cGPR@0x%"PRIx64" = { ", "SV"[v_or_s], addr);
		for (x = 0; x < r; x += 4) {
			fprintf(asic->options.test_log_fd, "0x%"PRIx32, dst[x/4]);
			if (x < (r - 4))
				fprintf(asic->options.test_log_fd, ", ");
		}
		fprintf(asic->options.test_log_fd, "}\n");
	}

	if (v_or_s == 0) {
		// read trap if any
		if (umr_wave_data_get_flag_trap_en(asic, wd) || umr_wave_data_get_flag_priv(asic, wd)) {
			lseek(asic->fd.gprwave, 4 * 0x6C, SEEK_SET);
			r = read(asic->fd.gprwave, &dst[0x6C], 4 * 16);
			if (r > 0) {
				if (asic->options.test_log && asic->options.test_log_fd) {
					int x;
					fprintf(asic->options.test_log_fd, "SGPR@0x%"PRIx64" = { ", addr + 0x6C * 4);
					for (x = 0; x < r; x += 4) {
						fprintf(asic->options.test_log_fd, "0x%"PRIx32, dst[0x6C + x/4]);
						if (x < (r - 4))
							fprintf(asic->options.test_log_fd, ", ");
					}
					fprintf(asic->options.test_log_fd, "}\n");
				}
			}
		}
	}

	return r;
}

/**
 * umr_read_sgprs - Read SGPR registers for a specific wave
 */
int umr_read_sgprs(struct umr_asic *asic, struct umr_wave_data *wd, uint32_t *dst)
{
	if (asic->fd.gprwave >= 0) {
		return read_gpr_gprwave(asic, 0, 0, wd, dst);
	} else {
		asic->err_msg("[ERROR]:  Your kernel is too old the amdgpu_gprwave file is now required.\n");
		return -1;
	}
}

int umr_read_vgprs(struct umr_asic *asic, struct umr_wave_data *wd, uint32_t thread, uint32_t *dst)
{
	// reading VGPR is not supported on pre GFX9 devices
	if (asic->family < FAMILY_AI)
		return -1;

	if (asic->fd.gprwave >= 0) {
		return read_gpr_gprwave(asic, 1, thread, wd, dst);
	} else {
		asic->err_msg("[ERROR]:  Your kernel is too old the amdgpu_gprwave file is now required.\n");
		return -1;
	}
}

int umr_get_wave_status(struct umr_asic *asic, unsigned se, unsigned sh, unsigned cu, unsigned simd, unsigned wave, struct umr_wave_status *ws)
{
	uint32_t buf[32];
	int r = 0;
	uint64_t addr = 0;
	struct amdgpu_debugfs_gprwave_iocdata id;

	memset(buf, 0, sizeof buf);

	if (asic->fd.gprwave >= 0) {
		memset(&id, 0, sizeof id);
		id.gpr_or_wave = 0;
		id.se = se;
		id.sh = sh;
		id.cu = cu;
		id.wave = wave;
		id.simd = simd;
		id.xcc_id = asic->options.vm_partition == -1 ? 0 : asic->options.vm_partition;

		r = ioctl(asic->fd.gprwave, AMDGPU_DEBUGFS_GPRWAVE_IOC_SET_STATE, &id);
		if (r)
			return r;

		lseek(asic->fd.gprwave, 0, SEEK_SET);
		r = read(asic->fd.gprwave, buf, 32*4);
		if (r < 0)
			return r;
	} else {
		asic->err_msg("[ERROR]:  Your kernel is too old the amdgpu_gprwave file is now required.\n");
		return -1;
	}

	if (asic->options.test_log && asic->options.test_log_fd) {
		int x;
		addr = ((uint64_t)id.se << 7) |
			   ((uint64_t)id.sh << 15) |
			   ((uint64_t)id.cu << 23) |
			   ((uint64_t)id.wave << 31) |
			   ((uint64_t)id.simd << 37);
		fprintf(asic->options.test_log_fd, "WAVESTATUS@0x%"PRIx64" = { ", addr);
		for (x = 0; x < r; x += 4) {
			fprintf(asic->options.test_log_fd, "0x%"PRIx32, buf[x/4]);
			if (x < (r - 4))
				fprintf(asic->options.test_log_fd, ", ");
		}
		fprintf(asic->options.test_log_fd, "}\n");
	}


	return umr_parse_wave_data_gfx(asic, ws, buf, r>>2);
}

int umr_get_wave_sq_info(struct umr_asic *asic, unsigned se, unsigned sh, unsigned cu, struct umr_wave_status *ws)
{
	return umr_get_wave_sq_info_vi(asic, se, sh, cu, ws);
}
